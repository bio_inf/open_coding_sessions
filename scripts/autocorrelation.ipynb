{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Autocorrelation\n",
    "\n",
    "In this tutorial we will learn about autocorrelation and the danger of ignoring it in statistical analyses.\n",
    "\n",
    "By fitting a statistical model, we typically want to distinguish a systematic part of a signal and a random part. Let us construct a systematic and a random signal.\n",
    "\n",
    "**Example 1**: Standard gaussian noise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timestep <- 0.01\n",
    "time <- seq(from=0,to=1,by=timestep)\n",
    "N <- length(time)\n",
    "signal_systematic <- 0.5*time\n",
    "signal_random <- rnorm(N,sd = 0.3) # random numbers, normally distributed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hist(signal_random)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(time,signal_systematic+signal_random)\n",
    "lines(time,signal_systematic,col=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see if a linear regression can find out what the systematic trend is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mydata <- data.frame(time=time,signal=signal_systematic+signal_random)\n",
    "mylm <- lm(signal~time,mydata)\n",
    "summary(mylm)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "beta = summary(mylm)$coef[\"time\",\"Estimate\"]\n",
    "beta_sd = summary(mylm)$coef[\"time\",\"Std. Error\"]\n",
    "print(paste0(\"the trend should be between \",beta-2*beta_sd,\" and \",beta+2*beta_sd))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example 2:** Gaussian noise at less points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timestep_noise <- 0.1\n",
    "time_noise <- seq(from=0,to=1,by=timestep_noise)\n",
    "N_noise <- length(time_noise)\n",
    "noise <- rnorm(N_noise,sd=0.3)\n",
    "plot(time_noise,noise)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We linearly interpolate the noise to the original signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_random <- approx(x=time_noise, y=noise, xout = time)$y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(time,signal_systematic+signal_random)\n",
    "lines(time,signal_systematic,col=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We repeat fitting the linear model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mydata <- data.frame(time=time,signal=signal_systematic+signal_random)\n",
    "mylm <- lm(signal~time,mydata,)\n",
    "summary(mylm)\n",
    "beta = summary(mylm)$coef[\"time\",\"Estimate\"]\n",
    "beta_sd = summary(mylm)$coef[\"time\",\"Std. Error\"]\n",
    "print(paste0(\"the trend should be between \",beta-2*beta_sd,\" and \",beta+2*beta_sd))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's assume we have no signal at all."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_systematic <- 0\n",
    "mydata <- data.frame(time=time,signal=signal_systematic+signal_random)\n",
    "mylm <- lm(signal~time,mydata)\n",
    "summary(mylm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try this a few times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for (i in 1:10) {\n",
    "    noise <- rnorm(N_noise,sd=0.3)\n",
    "    signal_random <- approx(x=time_noise, y=noise, xout = time)$y\n",
    "    mydata <- data.frame(time=time,signal=signal_systematic+signal_random)\n",
    "    mylm <- lm(signal~time,mydata)\n",
    "    beta = summary(mylm)$coef[\"time\",\"Estimate\"]\n",
    "    beta_sd = summary(mylm)$coef[\"time\",\"Std. Error\"]\n",
    "    print(paste0(\"the systematic trend should be between \",beta-2*beta_sd,\" and \",beta+2*beta_sd))\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Maybe it helps to increase the resolution?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timestep <- 1/1000\n",
    "time <- seq(from=0,to=1,by=timestep)\n",
    "for (i in 1:10) {\n",
    "    noise <- rnorm(N_noise,sd=0.3)\n",
    "    signal_random <- approx(x=time_noise, y=noise, xout = time)$y\n",
    "    mydata <- data.frame(time=time,signal=signal_systematic+signal_random)\n",
    "    mylm <- lm(signal~time,mydata)\n",
    "    beta = summary(mylm)$coef[\"time\",\"Estimate\"]\n",
    "    beta_sd = summary(mylm)$coef[\"time\",\"Std. Error\"]\n",
    "    print(paste0(\"the systematic trend should be between \",beta-2*beta_sd,\" and \",beta+2*beta_sd))\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oh no! Increasing the time resolution made it almost certain that we detect a systematic trend where there is none. The reason is temporal autocorrelation.\n",
    "\n",
    "## How to deal with autocorrelation\n",
    "\n",
    "The first step is to detect it. Let's go back to the original problem (with a time step of 1/100) because otherwise our calculations may take long."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timestep <- 1/100\n",
    "time <- seq(from=0,to=1,by=timestep)\n",
    "signal_systematic <- 0\n",
    "noise <- rnorm(N_noise,sd=0.3)\n",
    "signal_random <- approx(x=time_noise, y=noise, xout = time)$y\n",
    "mydata <- data.frame(time=time,signal=signal_systematic+signal_random)\n",
    "plot(mydata$time, mydata$signal)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mylm <- lm(signal~time,mydata)\n",
    "summary(mylm)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "acf(resid(mylm))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(car)\n",
    "durbinWatsonTest(mylm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### correcting the model for autocorrelation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(nlme)\n",
    "mygls <- gls(signal~time,mydata,correlation = corAR1(form = ~1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "summary(mygls)\n",
    "acf(resid(mygls,type=\"normalized\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often it helps. But if not, we need to resample the time series:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head(mydata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mydata$time_rounded <- round(time / 0.05) * 0.05\n",
    "rounded_data <- aggregate(mydata, by=list(mydata$time_rounded), FUN=mean)\n",
    "plot(rounded_data$time, rounded_data$signal)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mylm <- lm(signal~time,rounded_data)\n",
    "summary(mylm)\n",
    "acf(resid(mylm))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mygls <- gls(signal~time,rounded_data,correlation = corAR1(form = ~1))\n",
    "summary(mygls)\n",
    "acf(resid(mygls,type=\"normalized\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"######## mylm #############\")\n",
    "summary(mylm)\n",
    "print(\"######## mygls #############\")\n",
    "summary(mygls)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "(3) R",
   "language": "R",
   "name": "iow_r"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
