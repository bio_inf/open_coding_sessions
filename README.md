This repository is intended to collect the solutions for coding and data analysis questions that came up during the open coding sessions or elsewhere and might be of interest to a larger group of people. As messages in the [rocket.chat channel](https://chat.io-warnemuende.de/channel/open-coding-sessions) are deleted after a while, this repository may offer a more long-term documentation. Also the issue tracker can and should be used to ask questions. The repository is public and not intended to share (large) data sets.

To add an example to the repository, please adhere to the following rules: 
* interactive scripts are collected in the folder [scripts](https://git.io-warnemuende.de/bio_inf/open_coding_sessions/src/branch/master/scripts)
* figures are collected in the folder [images](https://git.io-warnemuende.de/bio_inf/open_coding_sessions/src/branch/master/images) as image files (e.g. png)
* scripts to be sourced are collected in the folder [src](https://git.io-warnemuende.de/bio_inf/open_coding_sessions/src/branch/master/src)
* each example is accompanied by a page on the wiki and all required materials and output (scripts, sources, figures) are linked there
* if examples include a dummy data set, this can be saved in [data](https://git.io-warnemuende.de/bio_inf/open_coding_sessions/src/branch/master/data)
* if a presentation was prepared for the open coding session, a pdf of the presentation should be added to [slides](https://git.io-warnemuende.de/bio_inf/open_coding_sessions/src/branch/master/slides)

